const myConnection = require('./Query');
const logger = require('./logger');
const nodemailer = require('nodemailer');
const setting = require('./Settings');
const mysql = require('mysql2');

// fungsi untuk mengirim email
async function sendEmail(id, from, to, cc, subject, ebody) {
    // konfigurasi email
    const data = await myConnection.getEmailAccount(from);

    var tls;
    if (data.tls == '1') tls = true;
    else tls = false;

    logger("Define.Account.Structure.");
    logger("Host=" + data.host);
    logger("Port=" + data.port);
    logger("Secure=" + tls);
    logger("Username=" + data.username);

    const transporter = nodemailer.createTransport({
        host: data.host,
        port: data.port,
        secure: tls,
        auth: {
            user: data.username,
            pass: data.password
        }
    });

    logger("Define.Email.Structure.");
    const myAttachment = await myConnection.getAttachmentInbound(id);
    logger(`attachment.data=${JSON.stringify(myAttachment)}`)
    const mailOptions = {
        from: from,
        to: to,
        cc: cc,
        subject: subject,
        html: ebody,
        attachments: myAttachment
    };

    transporter.sendMail(mailOptions, function (error, info) {
        let status = 0;
        let message = '';
        if (error) {
            logger("ERROR!" + error.message);
            status = 0;
            message = error.message;
        } else {
            logger('Email.Sent=' + info.response);
            status = 1;
            message = info.response;
        }
        myConnection.updateEmailOutStatus(id, status, message);
    });
}

function getEmailOut() {
    logger("Email.Out.Retrieve.Setting.Connection.from.Config.");
    const connection = mysql.createConnection(setting);

    logger("Query.get.Email.Out.");
    const data = connection.query("select id,eto,efrom,ecc,esubject,ebody_html from email_out where sent='0' order by date_email asc Limit 1", (err, rows) => {
        if (err) throw err;
        if (rows.length > 0) {
            for (let i = 0; i < rows.length; i++) {
                const id = rows[i].id;
                const eto = rows[i].eto;
                const efrom = rows[i].efrom;
                const ecc = rows[i].ecc;
                const esubject = rows[i].esubject;
                const ebody_html = rows[i].ebody_html;

                logger("Get.New.Data");
                logger(`ID=${id}`);
                logger(`To=${eto}`);
                logger(`From=${efrom}`);
                logger(`CC=${ecc}`);
                logger(`Subject=${esubject}`);
                logger(`Body=${ebody_html}`);

                sendEmail(id, efrom, eto, ecc, esubject, ebody_html);
            }
        }
    });

    connection.end((error) => {
        if (error) logger("ERROR!:getEmailOut.End.Database.Connection");

        logger("getEmailOut.End.Database.Connection")
    });

    setTimeout(getEmailOut, 10000);
}

module.exports = {
    getEmailOut
}